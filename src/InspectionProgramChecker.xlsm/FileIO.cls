VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "FileIO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private ExportData_() As String
Const kItemNum = 7
Const kSingleRecordNum = 3

Public Sub Export(Map As Map)
    Alloc ExportData_, Map.Size
    Format ExportData_, Map.Sets
    Sheet1.Range(Cells(1, 1), Cells(Map.Size, kItemNum * kSingleRecordNum + 1)) = ExportData_
End Sub

Private Sub Alloc(ExportData() As String, Size As Integer)
    ReDim ExportData(Size, kItemNum * kSingleRecordNum)
End Sub

Private Sub Format(ExportData() As String, Sets() As SingleRecordSet)
    Dim Index As Integer
    For Index = 0 To UBound(Sets)
        SetItems ExportData, Sets(Index).Spec, Index, kItemNum * 0
        SetItems ExportData, Sets(Index).Res1, Index, kItemNum * 1
        SetItems ExportData, Sets(Index).Res2, Index, kItemNum * 2
        If Sets(Index).IsOk Then
            ExportData(Index, kItemNum * kSingleRecordNum) = "�Z"
        End If
    Next Index
End Sub

Private Sub SetItems(ExportData() As String, SingleRecord As SingleRecord, RowIndex As Integer, ColumnIndex As Integer)
    ExportData(RowIndex, ColumnIndex) = SingleRecord.Item1
    ExportData(RowIndex, ColumnIndex + 1) = SingleRecord.Item2
    ExportData(RowIndex, ColumnIndex + 2) = SingleRecord.Item3
    ExportData(RowIndex, ColumnIndex + 3) = SingleRecord.Item4
    ExportData(RowIndex, ColumnIndex + 4) = SingleRecord.Item5
    ExportData(RowIndex, ColumnIndex + 5) = SingleRecord.Item6
    ExportData(RowIndex, ColumnIndex + 6) = SingleRecord.Item7
End Sub


