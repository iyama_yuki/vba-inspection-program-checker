Attribute VB_Name = "Main"
Option Explicit

Public Sub Main()
    Dim StartTime As Double
    StartTime = Timer

    Dim Record As New Record
    Record.Import
    Dim Map As New Map
    Map.CreateFrom Record
    Dim IO As New FileIO
    IO.Export Map
    
    Dim EndTime As Double
    EndTime = Timer
    Dim ProcessTime As Double
    ProcessTime = EndTime - StartTime
    Debug.Print "sec: " & ProcessTime
End Sub
