VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Record"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Specs_() As SingleRecord
Private Res1s_() As SingleRecord
Private Res2s_() As SingleRecord

Public Property Get SpecAt(Index As Integer) As SingleRecord
    Set SpecAt = Specs_(Index)
End Property

Public Property Get Res1s() As SingleRecord()
    Res1s = Res1s_
End Property

Public Property Get Res2s() As SingleRecord()
    Res2s = Res2s_
End Property

Public Property Get Size() As Integer
    Size = UBound(Specs_)
End Property

Public Sub Import()
    Const kDataSize = 10
    SpecAlloc Specs_, kDataSize
    ResAlloc Res1s_, kDataSize
    ResAlloc Res2s_, kDataSize
    
    ' FIXME: ダミーデータではなく、ファイルから読み込むようにする
    MyDebug.SetDummyData Specs_, Res1s_, Res2s_
End Sub

Private Sub SpecAlloc(Records() As SingleRecord, Size As Integer)
    ReDim Records(Size)
    Dim Index
    For Index = 0 To Size
        Set Records(Index) = New SpecSingleRecord
    Next Index
End Sub

Private Sub ResAlloc(Records() As SingleRecord, Size As Integer)
    ReDim Records(Size)
    Dim Index
    For Index = 0 To Size
        Set Records(Index) = New ResSingleRecord
    Next Index
End Sub




