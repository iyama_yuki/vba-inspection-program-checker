VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "SingleRecordSet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Spec_ As SingleRecord
Private Res1_ As SingleRecord
Private Res2_ As SingleRecord
Private IsOk_ As Boolean

Public Property Set Spec(Spec As SingleRecord)
    Set Spec_ = Spec
End Property

Public Property Get Spec() As SingleRecord
    Set Spec = Spec_
End Property

Public Property Set Res1(Res1 As SingleRecord)
    Set Res1_ = Res1
End Property

Public Property Get Res1() As SingleRecord
    Set Res1 = Res1_
End Property

Public Property Set Res2(Res2 As SingleRecord)
    Set Res2_ = Res2
End Property

Public Property Get Res2() As SingleRecord
    Set Res2 = Res2_
End Property

Public Property Get IsOk() As Boolean
    Dim Tmp As Boolean
    Tmp = Spec_.Valid

    If Res1.Valid Then
        Tmp = Tmp And _
        (Spec_.Item5 = Res1_.Item5 And _
        Spec_.Item6 = Res1_.Item6 And _
        Spec_.Item7 = Res1_.Item7)
    End If
    
    If Res2.Valid Then
        Tmp = Tmp And _
        (Spec_.Item5 = Res2_.Item5 And _
        Spec_.Item6 = Res2_.Item6 And _
        Spec_.Item7 = Res2_.Item7)
    End If
    
    IsOk = Tmp
End Property

