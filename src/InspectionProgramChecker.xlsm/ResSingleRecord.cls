VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ResSingleRecord"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements SingleRecord

Private Item1_ As String
Private Item2_ As String
Private Item3_ As String
Private Item4_ As String
Private Item5_ As String
Private Item6_ As String
Private Item7_ As String
Private Valid_ As Boolean

Private Sub Class_Initialize()
    Valid_ = True
End Sub

Private Property Let SingleRecord_Item1(ByVal Item1 As String)
    Item1_ = Item1
End Property

Private Property Get SingleRecord_Item1() As String
    SingleRecord_Item1 = Item1_
End Property

Private Property Let SingleRecord_Item2(ByVal Item2 As String)
    Item2_ = Item2
End Property

Private Property Get SingleRecord_Item2() As String
    SingleRecord_Item2 = Item2_
End Property

Private Property Let SingleRecord_Item3(ByVal Item3 As String)
    Item3_ = Item3
End Property

Private Property Get SingleRecord_Item3() As String
    SingleRecord_Item3 = Item3_
End Property

Private Property Let SingleRecord_Item4(ByVal Item4 As String)
    Item4_ = Item4
End Property

Private Property Get SingleRecord_Item4() As String
    SingleRecord_Item4 = Item4_
End Property

Private Property Let SingleRecord_Item5(ByVal Item5 As String)
    Item5_ = Item5
End Property

Private Property Get SingleRecord_Item5() As String
    SingleRecord_Item5 = Replace(Item5_, "H", "")
End Property

Private Property Let SingleRecord_Item6(ByVal Item6 As String)
    Item6_ = Item6
End Property

Private Property Get SingleRecord_Item6() As String
    SingleRecord_Item6 = Replace(Item6_, "H", "")
End Property

Private Property Let SingleRecord_Item7(ByVal Item7 As String)
    Item7_ = Item7
End Property

Private Property Get SingleRecord_Item7() As String
    Dim Tmp As String
    Tmp = Item7_
    Tmp = Replace(Tmp, "(", "")
    Tmp = Replace(Tmp, ")", "")
    Tmp = Replace(Tmp, "��", "u")
    Tmp = StrConv(Tmp, vbLowerCase)
    SingleRecord_Item7 = Tmp
End Property

Private Property Let SingleRecord_Valid(ByVal Valid As Boolean)
    Valid_ = Valid
End Property

Private Property Get SingleRecord_Valid() As Boolean
    SingleRecord_Valid = Valid_
End Property
