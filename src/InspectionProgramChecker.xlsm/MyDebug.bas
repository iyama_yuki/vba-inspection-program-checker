Attribute VB_Name = "MyDebug"
Option Explicit

Public Sub PrintSets(Sets() As SingleRecordSet, Optional Message As String = "Display Sets")
    Dim Index As Integer
    Debug.Print "=========================================="
    Debug.Print Message
    For Index = 0 To UBound(Sets)
        Debug.Print "------------------------------------------"
        If (Sets(Index).IsOk) Then
            Debug.Print "IsOk"
        End If
        Debug.Print _
        "SPEC:[" & Index & "]" & vbTab & _
        " 1:[" & Sets(Index).Spec.Item1 & "]" & vbTab & _
        " 2:[" & Sets(Index).Spec.Item2 & "]" & vbTab & _
        " 3:[" & Sets(Index).Spec.Item3 & "]" & vbTab & _
        " 4:[" & Sets(Index).Spec.Item4 & "]" & vbTab & _
        " 5:[" & Sets(Index).Spec.Item5 & "]" & vbTab & _
        " 6:[" & Sets(Index).Spec.Item6 & "]" & vbTab & _
        " 7:[" & Sets(Index).Spec.Item7 & "]" & vbTab & _
        " V:[" & Sets(Index).Spec.Valid & "] "
        Debug.Print _
        "RES1:[" & Index & "]" & vbTab & _
        " 1:[" & Sets(Index).Res1.Item1 & "]" & vbTab & _
        " 2:[" & Sets(Index).Res1.Item2 & "]" & vbTab & _
        " 3:[" & Sets(Index).Res1.Item3 & "]" & vbTab & _
        " 4:[" & Sets(Index).Res1.Item4 & "]" & vbTab & _
        " 5:[" & Sets(Index).Res1.Item5 & "]" & vbTab & _
        " 6:[" & Sets(Index).Res1.Item6 & "]" & vbTab & _
        " 7:[" & Sets(Index).Res1.Item7 & "]" & vbTab & _
        " V:[" & Sets(Index).Res1.Valid & "] "
        Debug.Print _
        "RES2:[" & Index & "]" & vbTab & _
        " 1:[" & Sets(Index).Res2.Item1 & "]" & vbTab & _
        " 2:[" & Sets(Index).Res2.Item2 & "]" & vbTab & _
        " 3:[" & Sets(Index).Res2.Item3 & "]" & vbTab & _
        " 4:[" & Sets(Index).Res2.Item4 & "]" & vbTab & _
        " 5:[" & Sets(Index).Res2.Item5 & "]" & vbTab & _
        " 6:[" & Sets(Index).Res2.Item6 & "]" & vbTab & _
        " 7:[" & Sets(Index).Res2.Item7 & "]" & vbTab & _
        " V:[" & Sets(Index).Res2.Valid & "] "
    Next Index
    Debug.Print "=========================================="
End Sub

Public Sub PrintSingleRecords(SingleRecords() As SingleRecord, Optional Message As String = "Display SingleRecords")
    Debug.Print "=========================================="
    Debug.Print Message
    Dim Index As Integer
    For Index = 0 To UBound(SingleRecords)
        Debug.Print _
        "[" & Index & "]" & vbTab & _
        " 1:[" & SingleRecords(Index).Item1 & "]" & vbTab & _
        " 2:[" & SingleRecords(Index).Item2 & "]" & vbTab & _
        " 3:[" & SingleRecords(Index).Item3 & "]" & vbTab & _
        " 4:[" & SingleRecords(Index).Item4 & "]" & vbTab & _
        " 5:[" & SingleRecords(Index).Item5 & "]" & vbTab & _
        " 6:[" & SingleRecords(Index).Item6 & "]" & vbTab & _
        " 7:[" & SingleRecords(Index).Item7 & "]" & vbTab & _
        " V:[" & SingleRecords(Index).Valid & "] "
    Next Index
    Debug.Print "=========================================="
End Sub

Public Sub ClearConsole(Optional Message As String = "Start:")
    Dim Index As Integer
    For Index = 1 To 20
        Debug.Print Chr(8)
    Next Index
    Debug.Print Message
End Sub

Public Sub SetDummyData(Specs() As SingleRecord, Res1s() As SingleRecord, Res2s() As SingleRecord)
    Specs(0).Item2 = "1"
    Specs(1).Item2 = "3"
    Specs(2).Item2 = "3"
    With Specs(3)
        .Item2 = "3"
        .Item4 = "step3"
        .Item5 = "1"
        .Item6 = "2"
        .Item7 = "ms"
    End With
    Specs(4).Item2 = "4"
    With Specs(5)
        .Item2 = "4"
        .Item4 = "step4"
        .Item5 = "10"
        .Item6 = "20"
        .Item7 = "ua"
    End With
    With Specs(6)
        .Item2 = "5"
        .Item4 = "step5"
        .Item5 = "1EH"
        .Item6 = "2FH"
        .Item7 = "HEX"
    End With
    With Specs(7)
        .Item1 = "X"
        .Item2 = "6"
        .Item4 = "step6"
    End With
    With Specs(8)
        .Item2 = "7"
        .Item4 = "step7"
    End With
    With Specs(9)
        .Item2 = "8"
        .Item4 = "step8"
    End With
    With Specs(10)
        .Item2 = "9"
        .Item4 = "step9"
    End With
    
    With Res1s(0)
        .Item2 = "3"
        .Item4 = "step3"
        .Item5 = "1"
        .Item6 = "2"
        .Item7 = "(MS)"
    End With
    With Res1s(1)
        .Item2 = "4"
        .Item4 = "step4"
        .Item5 = "10"
        .Item6 = "20"
        .Item7 = "��a"
    End With
    With Res1s(2)
        .Item2 = "5"
        .Item4 = "step5"
        .Item5 = "1EH"
        .Item6 = "2FH"
        .Item7 = "HEX"
    End With
    With Res1s(3)
        .Item2 = "7"
        .Item4 = "step7"
    End With
    With Res1s(4)
        .Item2 = "8"
        .Item4 = "step8"
    End With
    With Res1s(5)
        .Item2 = "10"
        .Item4 = "step10"
    End With
    With Res1s(6)
        .Item2 = "11"
        .Item4 = "step11"
    End With
    With Res1s(7)
        .Item2 = "12"
        .Item4 = "step12"
    End With

    With Res2s(0)
        .Item2 = "3"
        .Item4 = "step3"
        .Item5 = "1"
        .Item6 = "2"
        .Item7 = "(MS)"
    End With
    With Res2s(1)
        .Item2 = "5"
        .Item4 = "step5"
        .Item5 = "1EH"
        .Item6 = "2FH"
        .Item7 = "HEX"
    End With
    With Res2s(2)
        .Item2 = "7"
        .Item4 = "step7"
    End With
    With Res2s(3)
        .Item2 = "8"
        .Item4 = "step8"
    End With
    With Res2s(4)
        .Item2 = "9"
        .Item4 = "step9"
    End With
    With Res2s(5)
        .Item2 = "10"
        .Item4 = "step10"
    End With
    With Res2s(6)
        .Item2 = "11"
        .Item4 = "step11"
    End With
    With Res2s(7)
        .Item2 = "13"
        .Item4 = "step13"
    End With
End Sub
