VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Map"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Sets_() As SingleRecordSet

Public Property Get Sets() As SingleRecordSet()
    Sets = Sets_
End Property

Public Property Get SetAt(Index As Integer) As SingleRecordSet
    Set SetAt = Sets_(Index)
End Property

Public Property Get Size() As Integer
    Size = UBound(Sets_)
End Property

Public Sub CreateFrom(Record As Record)
    Alloc Sets_, Record.Size
    SetRecord Sets_, Record
End Sub

Private Sub Alloc(Sets() As SingleRecordSet, Size As Integer)
    ReDim Sets(Size)
    Dim Index
    For Index = 0 To Size
        Set Sets(Index) = New SingleRecordSet
    Next Index
End Sub

Private Sub SetRecord(Sets() As SingleRecordSet, Record As Record)
    Dim Index As Integer
    For Index = 0 To UBound(Sets)
        Set Sets(Index).Spec = Record.SpecAt(Index)
        Set Sets(Index).Res1 = PairDetection(Record.SpecAt(Index), Record.Res1s)
        Set Sets(Index).Res2 = PairDetection(Record.SpecAt(Index), Record.Res2s)
    Next Index
End Sub

Private Function PairDetection(Spec As SingleRecord, Results() As SingleRecord) As SingleRecord
    Dim NothingSingleRecord As SingleRecord
    Set NothingSingleRecord = New ResSingleRecord
    NothingSingleRecord.Valid = False
    
    If Not Spec.Valid Then
        Set PairDetection = NothingSingleRecord
        Exit Function
    End If
    
    Dim Index As Integer
    For Index = 0 To UBound(Results)
        If (IsPair(Spec, Results(Index))) Then
            Set PairDetection = Results(Index)
            Exit Function
        End If
    Next Index
    
    Set PairDetection = NothingSingleRecord
End Function

Private Function IsPair(Spec As SingleRecord, Result As SingleRecord) As Boolean
    IsPair = (Spec.Item2 = Result.Item2) And (Spec.Item4 = Result.Item4)
End Function
