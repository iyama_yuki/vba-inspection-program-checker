# vba-inspection-program-checker

## Development

### エクセルシートからソースコードに変換

wslの場合

```bash:
$ cscript.exe vbac.wsf decombine
```

### ソースコードからエクセルシートに変換

wslの場合

```bash:
$ cscript.exe vbac.wsf combine
```

windowsの場合

```bash:
$ build.bat
```